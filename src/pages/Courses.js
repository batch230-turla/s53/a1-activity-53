import {Fragment} from 'react';
import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'
export default function Courses(){
		console.log("Contents of coursesData")
		console.log(coursesData);
		console.log(coursesData[0]);
		//array method to display all courses

		const courses = coursesData.map(course =>{
			return(
				<CourseCard key = {course.id} courseProp = {course} />

				)

		})

	// return(
	// 	<React.Fragment>
	// 		<CourseCard courseProp= {coursesData[0]}/>
	// 	</React.Fragment>
		
	// 	)

	return (
		<Fragment>
			{courses}
		</Fragment>
		)
}