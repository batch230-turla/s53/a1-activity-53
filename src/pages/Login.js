import {Form, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';

//_____________________________________ACTIVITY 52________________________

export default function Login(){
	//State hooks to storee the values of input fields
	const [email,setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	//state to determine whether submit button is eenable or not 
	const [isActive, setIsActive] = useState(false);

	//check if values are successfully binded
	console.log(email);
	console.log(password1);
	

	function registerUser(event){

		//Prevent page redirection via form submission
		// event.preventDefault();

		//clear input fields
		localStorage.setItem('email',email);
		setEmail('');
		setPassword1('');
		
		alert('Salamat! Isa ka nang Tunay na Rehistrado');

	}

	useEffect(()=>{
		if ((email !=='' && password1 !=='' ) && (password1 === password1)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[email,password1])



	return (
		<Form onSubmit={(event)=> registerUser(event)}>

		<h3>Login Page</h3>

		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value = {email}
			                onChange = {e => setEmail(e.target.value)}
			                required	
		                />
		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value = {password1}
			                onChange = {e => setPassword1(e.target.value)} 
			                required
		                />
		            </Form.Group>

		            {isActive ? // true //conditional render
		            	<Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>
		            : //false
		            <Button variant="secondary" type="submit" id="submitBtn " disabled>
		            	Submit
		            </Button>

		        	}
		           
		        </Form>
		)
}