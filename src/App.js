// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
// import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/ // s53 to comment
import { BrowserRouter as Router } from 'react-router-dom'; // s53 added
import { Route, Routes } from 'react-router-dom'; //s53 added

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';


function App() {
  return (
    /* React Fragments allows us to return multiple elements*/
    <Router>
      
      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </Container>
    </ Router>

    /*
      <Router>
    <Routes>
        <Route path ="/yourDesiredPath" element{<Component /> } />
    </Routes>
  
      </Router>
    */
  );
}

export default App;
